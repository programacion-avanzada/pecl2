/*
 * Este archivo es la clase exposicion
 * incluye todas las funciones que pueden hacer los visitantes, así como parar
 * y reanudar la exposición
 */
package PECL2;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

public class Exposicion
{
    int aforo;
    ListaThreads colaEspera, dentro;
    Semaphore semaforo;
    volatile boolean parado;
    
    public Exposicion(int aforo, JTextField tfEsperan, JTextField tfDentro)
    {
        this.aforo = aforo;
        semaforo = new Semaphore(aforo,true);
        colaEspera = new ListaThreads(tfEsperan);
        dentro = new ListaThreads(tfDentro);
        parado = false;
    }
    
    public void entrar(Visitante v)
    {
        monitor();
        colaEspera.meter(v);
        try
        {
            semaforo.acquire();
        } catch(InterruptedException e){ }

        colaEspera.sacar(v);
        dentro.meter(v);
        
    }

    public void salir(Visitante v)
    {
        monitor();
        dentro.sacar(v);
        semaforo.release();
    }
    
    public void mirar(Visitante v)
    {
        monitor();
            try
            {
                Thread.sleep(2000+(int)(3000*Math.random()));
            } catch (InterruptedException e){ }
    }
    
    public synchronized void monitor(){
        
        while(parado){
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(Exposicion.class.getName()).log(Level.SEVERE, null, ex);   //

            }
        }
    }
    
    public void parar(){
        parado=true;
    }
    
    public synchronized void reanudar(){
        parado=false;
        notifyAll();
    }

}
