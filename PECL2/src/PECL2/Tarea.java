/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PECL2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author peta_zetas
 */
public class Tarea extends Thread{
    
    private Socket socket;
    private Exposicion exposicion;

    public Tarea(Exposicion exposicion, Socket socket) {
        this.exposicion = exposicion;
        this.socket = socket;
    }

    public void run(){
        
        DataOutputStream salida;
        DataInputStream entrada;
                   
        while(true){
            
            try{
                
                //socket = server.accept();                                   //Esperamos la conexión
                entrada = new DataInputStream(socket.getInputStream());     //Abrimos canales Entrada
                salida = new DataOutputStream(socket.getOutputStream());    //Abrimos canales Salida
                
                String mensaje = entrada.readUTF();                         //Leemos el mensaje del cliente
                System.out.println(mensaje);
                
                if(mensaje.equals("Detener")){                               
                    System.out.println("Detener server");
                    exposicion.parar();

                }
                
                else if(mensaje.equals("Reanudar")){
                    System.out.println("Reanudar server");
                    exposicion.reanudar();
                }
                
                else{
                    System.out.println("Ha habido un error al recibir el mensaje");
                }
                
                //socket.close();                                             //Se cierra la conexion
            }catch(IOException e){}
        }        
    }
    
}
