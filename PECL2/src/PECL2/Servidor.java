/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PECL2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author peta_zetas
 */
public class Servidor extends Thread{
    
    private Exposicion exposicion;
    private ExecutorService pool = Executors.newFixedThreadPool(10); 
    private ServerSocket server;



    public Servidor(Exposicion exposicion) {
        this.exposicion = exposicion;
        try {
            server = new ServerSocket(5000);
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void responde() throws IOException{
        
        Socket socket;
        DataOutputStream salida;
        DataInputStream entrada;
        
        Socket socket2 = server.accept();                                               //Escucha para una conexion
        Tarea tarea = new Tarea(exposicion, socket2);
        pool.execute(tarea);
        
    }
    public void run() {
        while(true){
            try {
                responde();
            
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    }
    
}

